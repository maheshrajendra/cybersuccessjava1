package com.mr.samplestaticvariables;

public class Student {
	String name;
	int age;
	static String collegeName = "Oxford";

	public static void main(String[] args) {
		System.out.println(Student.collegeName); // Oxford
		Student s1 = new Student();
		s1.name = "Alpha";
		s1.age = 20;
		System.out.println(s1.collegeName);
		
		Student s2 = new Student();
		s2.name = "Beta";
		s2.age = 25;
		System.out.println(s1.collegeName); // Oxford
		System.out.println(s2.collegeName); // Oxford
		
		Student.collegeName = "XYZIT";
		System.out.println(s1.collegeName); // XYZIT
		System.out.println(s2.collegeName); // XYZIT
 	}
}
