
public class ForEachLoopExample {
	public static void main(String[] args) {
		int[] nums = { 10, 20, 30, 40, 50 };
		for (int num : nums) {
			System.out.println(num);
		}

		System.out.println("_______________");

		String[] names = { "Alpha", "Beta", "Charlie" };
		for (String name : names) {
			System.out.println(name);
		}

		System.out.println("+++++++++++++++++");

		char[] symbols = { '*', '%', '#', '&' };
		for (char c : symbols) {
			System.out.println(c);
		}
	}
}
