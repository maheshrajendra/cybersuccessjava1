
public class SampleGreeting {
	static void greet(char input) {
		switch (input) {
		case 'A':
			System.out.println("Afternoon");
			break;
		case 'M':
			System.out.println("Morning");
			break;
		case 'E':
			System.out.println("Evening");
			break;
		case 'N':
			System.out.println("Night");
			break;
		default:
			System.out.println("Good day!!!");
		}
	}
	
	public static void main(String[] args) {
		greet('M');
	}
}
