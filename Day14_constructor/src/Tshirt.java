
public class Tshirt {

	String color;
	char size;

	// ALT + Shift + S + O -> generate constructor using fields
	Tshirt(String color, char size) {
		this.color = color;
		this.size = size;
	}
	
	public static void main(String[] args) {
		Tshirt t = new Tshirt("Yellow", 'M');
		System.out.println(t.color);
		System.out.println(t.size);
		
		System.out.println(t); // it prints address of the object
	}
}
