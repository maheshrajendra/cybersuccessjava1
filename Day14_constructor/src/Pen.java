
public class Pen {
	String color;

	Pen(String color) {
		this.color = color;
	}

	public static void main(String[] args) {
		Pen p1 = new Pen("Red");
		System.out.println(p1.color);

		Pen p2 = new Pen("Green");
		System.out.println(p2.color);

		Pen p3 = new Pen("Blue");
		System.out.println(p3.color);
	}
}
