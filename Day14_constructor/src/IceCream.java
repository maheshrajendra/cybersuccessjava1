
public class IceCream {
	int price;
	char size;
	String flavour;

	// Alt + shift + s + o
	IceCream(String flavour, int price, char size) {
		this.flavour = flavour;
		this.price = price;
		this.size = size;
	}

	IceCream(String flavour) {
		this.flavour = flavour;
	}

	public static void main(String[] args) {
		IceCream i1 = new IceCream("Chocolate");
		System.out.println(i1.flavour); // Chocolate
		System.out.println(i1.price); // 0
		System.out.println(i1.size); // '\u0000'

		IceCream i2 = new IceCream("Butterscotch", 100, 'L');
		System.out.println(i2.price);// 100

		IceCream i3 = new IceCream("Vanilla", 60, 'S');
		System.out.println(i3.size); // S
	}

}
