
public class ReverseUsingStringBuilder {
	public static void main(String[] args) {
		String sentence = "This is java";
		String rev = new StringBuilder(sentence).reverse().toString();
		System.out.println(rev);
	}
}
