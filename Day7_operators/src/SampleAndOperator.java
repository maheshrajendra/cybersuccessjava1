
public class SampleAndOperator {
	public static void main(String[] args) {
		int x = 15;
		int y = 20;
		boolean result = ++x <= 15 && ++y >= 20;
		System.out.println(result);
		System.out.println(x);
		System.out.println(y);
	}
}

// && -> is a logical AND operator, if first condition is false,
// then it will never execute the second condition.

// & -> is a bitwise AND Operator, if first condition is false,
// still it will execute the second condition.
