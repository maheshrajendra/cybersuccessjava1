
public class SampleOROperator {
	public static void main(String[] args) {
		int x = 14;
		int y = 13;
		boolean result = x++ >= ++y | ++x >= y++;
		System.out.println(result);
		System.out.println(x);
		System.out.println(y);
	}
}
