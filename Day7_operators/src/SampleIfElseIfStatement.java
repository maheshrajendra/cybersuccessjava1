
public class SampleIfElseIfStatement {
	public static void main(String[] args) {
		int avg = 92;
		if (avg >= 85) {
			System.out.println("Distinction");
		} else if (avg >= 75 && avg < 85) {
			System.out.println("First class");
		} else if (avg >= 60 && avg < 75) {
			System.out.println("Second class");
		} else if (avg >= 35 && avg < 60) {
			System.out.println("Pass class");
		} else {
			System.out.println("fail");
		}
		
	}
}
