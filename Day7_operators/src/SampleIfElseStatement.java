
public class SampleIfElseStatement {
	public static void main(String[] args) {
		int age = 23;
		char gender = 'F';
		if (age >= 21 && gender == 'M') {
			System.out.println("eligible for vote");
		} else {
			System.out.println("not eligible to vote");
		}
		
		// if condition returns true, then if part will execute
		// if condition returns false, then else part will execute
	}
}
