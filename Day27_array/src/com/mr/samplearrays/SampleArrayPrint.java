package com.mr.samplearrays;

import java.util.Arrays;
public class SampleArrayPrint {
	public static void main(String[] args) {
		// dynamic array initialization
		int[] nums = { 10, 20, 30, 40 };
		System.out.println(nums.length); // 4
		System.out.println(nums[2]); // 30
		System.out.println(Arrays.toString(nums)); // [10,20,30,40]

		// logic for retrieving elements and adding 100 to each one.
		System.out.println("_______________");
		for (int i : nums) {
			System.out.println(i + 100);
		}
		
	}
}
