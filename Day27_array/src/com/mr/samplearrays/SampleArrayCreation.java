package com.mr.samplearrays;

import java.util.Arrays;

public class SampleArrayCreation {
	public static void main(String[] args) {
		String[] colors = new String[5];
		System.out.println(colors);
		System.out.println(Arrays.toString(colors));
		colors[0] = "Red";
		colors[1] = "Green";
		colors[2] = "Blue";
		colors[3] = "White";
		colors[4] = "Black";
		System.out.println(Arrays.toString(colors));
		System.out.println(colors.length); // 5
	}
}
