
public interface SampleInterface {
	int X = 10;

	void show();

	// 1.8
	static void add(int x, int y) {

	}

	// 1.8
	default void print() {

	}

}
