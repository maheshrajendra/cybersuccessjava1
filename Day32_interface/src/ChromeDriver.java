
public class ChromeDriver implements WebDriver {

	@Override
	public void close() {
		System.out.println("Chrome browser is closed");
	}

	@Override
	public void getTitle() {
		System.out.println("Returning the title of the page");
	}

	@Override
	public void get(String url) {
		System.out.println("opening " + url + " in chrome browser and"
				+ " waits until the page is loaded");
	}

}
