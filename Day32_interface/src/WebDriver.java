
public interface WebDriver {
	void get(String url);

	void close();

	void getTitle();
}
