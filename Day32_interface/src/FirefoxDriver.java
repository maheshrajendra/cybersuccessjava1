
public class FirefoxDriver implements WebDriver {

	@Override
	public void get(String url) {
		System.out.println("Opens " + url + " in the firefox browser");
	}

	@Override
	public void close() {
		System.out.println("Closes the firefox browser");
	}

	@Override
	public void getTitle() {
		System.out.println("Gets the title from the firefox browser opened url page");
	}

}
