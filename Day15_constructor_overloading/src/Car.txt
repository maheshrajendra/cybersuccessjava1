			Car.java

class Car {

	String color;
	char variant;
	String brand = "BMW";

	Car(String color){
		this.color = color;
		this.variant = 'E';
	}

	Car(String color, char variant){
		this.color = color;
		this.variant = variant;
	}

	public static void main(String[] args){
		Car c1 = new Car("Red);	
		
		Car c2 = new Car("Blue");	

		Car c3 = new Car("Black", 'P');

	}
}