package com.sample.jdbcexample;

public class MySql implements JDBC{

	@Override
	public void getConnection(String username, String password, int port) {
		System.out.println("Connected to mysql database");
	}

	@Override
	public void getTables() {
		System.out.println("Getting tables from mysql using "
				+ "show tables;");
	}



}
