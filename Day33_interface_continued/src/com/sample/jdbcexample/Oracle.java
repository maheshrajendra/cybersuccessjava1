package com.sample.jdbcexample;

public class Oracle implements JDBC {

	@Override
	public void getConnection(String username, String password, int port) {
		System.out.println("Connected to oracle database");
	}

	@Override
	public void getTables() {
		System.out.println("Getting data from oracle using "
				+ "select * from tab");
	}

}
