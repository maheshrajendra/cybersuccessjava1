import java.util.LinkedHashSet;

public class SampleHashSetClassExample {
	public static void main(String[] args) {
		LinkedHashSet set = new LinkedHashSet();
		set.add(10);
		set.add(20);
		set.add(20);
		set.add(null);
		set.add(null);
		set.add(5.6);
		System.out.println(set); //[10, 20, null, 5.6]
	}
}
