import java.util.ArrayList;
import java.util.Arrays;

public class UsingForEachOnCollections {
	public static void main(String[] args) {
		ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(4, 8, 2, 1, 6));
		
		for (int i : numbers) {
			System.out.println(i);
		}
	}
}
