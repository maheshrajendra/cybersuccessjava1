
import java.util.HashMap;

public class SampleHashMapExample {
	public static void main(String[] args) {
		HashMap h = new HashMap();
		h.put(3, "Alpha");
		h.put(4.5, 3);
		h.put("hello", true);
		System.out.println(h.size()); // 3
		System.out.println(h); // {4.5=3, 3=Alpha, hello=true}
		System.out.println(h.get(3)); // Alpha
		System.out.println(h.containsKey(4.5)); // true
		System.out.println(h.containsValue(5.6)); // false
		System.out.println(h.isEmpty()); // false
		
		h.clear(); // removes all the entries from the map object
		
		System.out.println(h); // {}
	}
}
