import java.util.ArrayList;
import java.util.Arrays;

public class Product {
	int productId;
	String name;

	public Product(int productId, String name) {
		this.productId = productId;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", name=" + name + "]";
	}

	public static void main(String[] args) {
		Product p1 = new Product(1234, "Mobile Phone");
		Product p2 = new Product(1567, "Laptop");

		ArrayList<Product> productList = new ArrayList<>(Arrays.asList(p1, p2));
		System.out.println(productList);
		
		// retrieve one by one object and print only its productId;
		for( Product p   :   productList ) {
			System.out.println(p.productId);
		}

	}
}
