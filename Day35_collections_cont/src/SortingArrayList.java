import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SortingArrayList {
	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<>(Arrays.asList("Mark", "Ted", "Parker"));
		System.out.println("Before sorting the data");
		System.out.println(names);


		Collections.sort(names);
		
		System.out.println("After sorting the data");
		System.out.println(names);
	}
}
