import java.util.Collections;
import java.util.TreeSet;

public class SampleTreeSet {
	public static void main(String[] args) {
		TreeSet t = new TreeSet(Collections.reverseOrder());
		t.add(45);
		t.add(20);
		t.add(20);
		t.add(36);
		t.add(1);
		t.add(28);
		System.out.println(t); // [1, 20, 28, 36, 45]
	}
}
