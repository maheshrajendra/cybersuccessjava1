import java.util.*;

public class SortNumbers {
	public static void main(String[] args) {
		ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(56, 1, 83, 25, 41, 67));
		System.out.println(nums);

		Collections.sort(nums, Collections.reverseOrder());

		System.out.println("After sorting the numbers");
		System.out.println(nums);
	}
}
