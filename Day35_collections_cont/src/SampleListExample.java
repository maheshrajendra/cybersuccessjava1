import java.util.ArrayList;
import java.util.Arrays;

public class SampleListExample {
	public static void main(String[] args) {
		ArrayList al = new ArrayList(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 98, 9, 7, 6, 5, 65));
		al.add(2);
		al.add(2);
		al.add(2);
		al.add(null);
		al.add(null);
		al.add(56);

		System.out.println(al);
		System.out.println(al.get(5));
	}
}
