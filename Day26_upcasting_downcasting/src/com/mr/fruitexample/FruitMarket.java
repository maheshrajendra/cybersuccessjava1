package com.mr.fruitexample;

public class FruitMarket {

	Fruit sellFruit(String fruitName) {
		if (fruitName != null) {
			if (fruitName.equalsIgnoreCase("Apple")) {
				return new Apple();
			} else if (fruitName.equalsIgnoreCase("orange")) {
				return new Orange();
			}
		}
		return null;
	}

	public static void main(String[] args) {
		FruitMarket f = new FruitMarket();
		Fruit fruit = f.sellFruit("apple"); // Fruit fruit = new Apple();
		// to avoid null pointer exception
		if (fruit != null) {
			fruit.clean();
			// to avoid class cast exception
			if (fruit instanceof Apple) {
				Apple a = (Apple) fruit;
				a.prepareMilkShake();
			} else if (fruit instanceof Orange) {
				Orange o = (Orange) fruit;
				o.prepareJuice();
			}
		} else {
			System.out.println("No such fruit in the shop");
		}

	}

}
