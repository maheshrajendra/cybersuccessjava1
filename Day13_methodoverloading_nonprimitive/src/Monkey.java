
public class Monkey {
	void eat(Apple a) {
		System.out.println("monkey ate apple");
	}

	void eat(Banana b) {
		System.out.println("Monkey ate banana");
	}

	void eat(CupCake c) {
		System.out.println("Monkey ate cupcake");
	}

	public static void main(String[] args) {
		Monkey m = new Monkey();
		Apple a = new Apple();
		m.eat(a);

		Banana kela = new Banana();
		m.eat(kela);
		
		m.eat(new CupCake());
		
		// m.eat(200); - compiler error because there is no method which accepts one argument of type int
		// compile time polymorphism: the compiler decides based on the no. of arguments and datatype of arguments,
		// which method implementation must be attached with the method call.
	}
}
