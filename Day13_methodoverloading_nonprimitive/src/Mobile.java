
public class Mobile {
	void unlock(int pin) {
		System.out.println("Unlocked the phone using pin number " + pin);
	}

	void unlock(String password) {
		System.out.println("Unlocked the phone using password " + password);
	}

	void unlock(FingerPrint f) {
		System.out.println("Unlocked phone using finger print");
	}

	public static void main(String[] args) {
		Mobile m = new Mobile();
		int pinNumber = 1234;
		m.unlock(pinNumber);

		String pwd = "Alpha@123";
		m.unlock(pwd);

		m.unlock(new FingerPrint());
	}
}
