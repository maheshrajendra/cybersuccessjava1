package com.mr.samplestringcreation;

public class Sample {
	public static void main(String[] args) {
		System.out.println("hello".compareTo("helloabcde")); // -5
		System.out.println("Abc".compareTo("Ddfsdff")); // -3
		System.out.println("HELLOA".compareTo("HELLOB"));// -1
		System.out.println("Helloabc".compareTo("Hellodefghi")); // -3
		System.out.println("Hello".compareTo("Hello")); // 0
		System.out.println("A".compareTo("B")); // -1
		System.out.println("B".compareTo("A")); // 1
	}
}
