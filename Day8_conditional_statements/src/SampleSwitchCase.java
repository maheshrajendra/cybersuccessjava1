
public class SampleSwitchCase {
	public static void main(String[] args) {
		String accountType = "rd";
		switch (accountType) {
		case "savings":
			System.out.println("min. balance is 1000rs");
			break;
		case "rd":
		case "current":
			System.out.println("min. balance is 10000rs");
			break;
		default:
			System.out.println("Not a valid type");
		}
		System.out.println("End of program");
	}
}
