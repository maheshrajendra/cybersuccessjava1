
public class Shark {
	// states - variables
	String color; // null
	double weight; // 0.0
	char size; // '\u0000'
	int noOfEyes = 2; // 2

	// behaviors - methods
	void swim() {
		System.out.println("Shark swim");
	}

	public static void main(String[] args) {
		Shark s1 = new Shark();
		s1.color = "Orange";
		s1.weight = 2.3;
		s1.size = 'L';
		// when you print object reference, we get address of
		// that object in hexadecimal value
		System.out.println(s1); // Shark@5e265ba4
		System.out.println(s1.color); // Orange
		System.out.println(s1.size); // L
		System.out.println(s1.weight); // 2.3
	}
}
