
public class TShirt {
	String color;
	char size;

	public static void main(String[] args) {
		TShirt t1 = new TShirt();
		TShirt t2 = new TShirt();
		System.out.println(t1.color); // null
		System.out.println(t1.size); // '\u0000'

		t1.color = "Blue";
		t2.color = "Red";
		System.out.println(t1.color); // Blue
		System.out.println(t2.color); // Red
	}
}
