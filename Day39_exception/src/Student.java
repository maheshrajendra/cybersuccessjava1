
public class Student {

	@Override
	protected void finalize() throws Throwable {
		System.out.println("Student object destroyed");
	}

	public static void main(String[] args) {
		Student s1 = new Student();
		Student s2 = new Student();
		Student s3 = new Student();
		Student s4 = new Student();
		s1 = null;
		s2 = null;
		s3 = null;
		s4 = null;
		System.gc();
	}
}
