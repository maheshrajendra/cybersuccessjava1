
public class SampleArithmeticException {
	public static void main(String[] args) {
		int x = 200;
		int y = 0;
		try {
			int z = x / y; // throw new ArithmeticException();
			System.out.println(z);
		} catch (ArithmeticException e) {
			e.printStackTrace();
		}
		System.out.println("Program completed");
	}
}
