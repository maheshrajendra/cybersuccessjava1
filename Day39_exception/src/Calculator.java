
public class Calculator {
	public static void main(String[] args) {
		// div(100, 0);
		// capitalizeWord(null);
		makeProgramSleepForSeconds(10000);
	}

	static void makeProgramSleepForSeconds(int i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	static void capitalizeWord(String word) {
		if (word != null) {
			System.out.println(word.toUpperCase());
		}
	}

	static void div(int i, int j) {
		try {
			System.out.println(i / j);
		} catch (Exception e) {
			System.out.println("some exception occured");
			e.printStackTrace();
		}
	}
}
