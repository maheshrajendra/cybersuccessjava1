
public class RunnerCommonUtility {
	public static void main(String[] args) {

		// static methods must be accessed using class name and dot operator
		boolean result = CommonUtility.isValid("abc@123");
		System.out.println(result);
	}
}
