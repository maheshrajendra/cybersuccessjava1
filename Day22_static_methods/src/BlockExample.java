
public class BlockExample {

	static {
		System.out.println("Static block 1");
	}

	{
		System.out.println("Instance block");
	}

	public static void main(String[] args) {
		System.out.println("main method");
		BlockExample b = new BlockExample();
		BlockExample b2 = new BlockExample();
		BlockExample b3 = new BlockExample();
	}
}

// Static block 1
// main method
// Instance block
// Instance block

// static block  -> if you want to execute some code before main method
// instance block -> if you want to execute some code before object creation.