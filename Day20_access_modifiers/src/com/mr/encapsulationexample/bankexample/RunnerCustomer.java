package com.mr.encapsulationexample.bankexample;

public class RunnerCustomer {
	public static void main(String[] args) {
		Customer c = new Customer();
		c.setAge(18);
		System.out.println(c.getAge());
		c.setName("Alpha");
		System.out.println(c.getName());
		c.setPhoneNo(98765432345L);
		System.out.println(c.getPhoneNo());

		System.out.println(c);
	}
}
