package com.mr.encapsulationexample.bankexample;

public class Customer {
	private String name;
	private int age;
	private long phoneNo;
	
	@Override
	public String toString() {
		return "Customer [name=" + name + ", age=" + age + ", phoneNo=" + phoneNo + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		if (age > 18) {
			this.age = age;
		}
	}

	public long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}

}
