package com.mr.encapsulationexample.productexample;

public class Product {
	private String name;
	private String brand;
	private String color;
	private double price;
	private long id;

	// alt + shift + s + r -> generate getter and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", brand=" + brand + ", color=" + color + ", price=" + price + ", id=" + id
				+ "]";
	}

}
