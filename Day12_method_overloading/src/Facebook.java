
public class Facebook {

	void login(String username, String password) {
		System.out.println("You logged in using username " + username);
	}

	void login(long phoneNum, String password) {
		System.out.println("You logged in using phoneNumber " + phoneNum);
	}

	public static void main(String[] args) {
		Facebook f = new Facebook();
		f.login("Mahesh@xyz.com", "Mahesh@123");

		f.login(9900339877L, "Mahesh@123");
	}

}
