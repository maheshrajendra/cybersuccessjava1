
public class Cat {
	String name;
	String color;

	@Override
	public String toString() {
		return "Cat [name=" + name + ", color=" + color + "]";
	}

	public static void main(String[] args) {
		Cat c = new Cat();
		System.out.println(c);
		c.name = "Kiw";
		c.color = "White";
		System.out.println(c);
		
		// indirectly when we print the object reference,
		// it will invoke toString() of that class, if we are not overriding,
		// then it will invoke the super class toString() implementation
	}
}
