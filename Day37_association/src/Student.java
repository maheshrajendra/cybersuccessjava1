
public class Student {
	int id;
	String name;
	Address address;
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", address=" + address + "]";
	}

	public static void main(String[] args) {
		Student s = new Student();
		s.id = 58;
		s.name = "Raj";
		s.address = new Address();
		System.out.println(s);
		s.address.houseNo = 25;
		s.address.city = "Mumbai";
		s.address.street = "1st main";
		s.address.pincode = 560000;
		System.out.println(s);
	}
}
