
public class Calculator {

	static void addition(int num1, int num2) {
		System.out.println(num1 + num2);
	}
	
	// you must write logic for square method -> assignment

	public static void main(String[] args) {
		addition(4, 5); // 9

		addition(20, 44); // 64

		addition(56, 78); // 134
		
		square(4); // 16
	}
}
