
public class Printer {

	// instead of creating object and calling with object reference
	// I am using static to call the method directly
	static void print() {
		System.out.println("hello");
	}

	public static void main(String[] args) {
		print();
		print();
		print();
		print();
	}
}
