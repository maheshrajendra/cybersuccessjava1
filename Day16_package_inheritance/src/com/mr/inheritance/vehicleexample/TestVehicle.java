package com.mr.inheritance.vehicleexample;

public class TestVehicle {

	public static void main(String[] args) {
		Bike b1 = new Bike();
		System.out.println(b1); // address of bike object -> com.mr.inheritance.vehicleexample.Bike@1175e2db
		System.out.println(b1.color); // null
		b1.color = "black";
		b1.brand = "BMW";
		b1.isKickerPresent = true;
		System.out.println(b1.color); // black

		Truck t = new Truck();
		t.start();
		t.stop();
		System.out.println(t);

		Car c = new Car();
		c.turnOnAc();
		System.out.println(c);

		Car c2 = new Car();
		c.start();
		c2.start();
		System.out.println(c2);
	}

}
