import java.util.ArrayList;

public class SampleWrapper {
	public static void main(String[] args) {
		int x = 10;
		// x. -> here you don't get any predefined methods as it is primitive

		Integer i = 10;
		// i. -> here you get lots of predefined method support as it is non primitive
		// wrapper classes

		// 2. in collections primitive datatypes are not supported, so we make use of 
		// wrapper classes.

	}
}
