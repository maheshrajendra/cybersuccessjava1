
public class SampleToString {
	public static void main(String[] args) {
		Integer i = 20;
		System.out.println(i); // 20

		Byte b = 10;
		System.out.println(b); // 10

		int value = Integer.parseInt("45");
		System.out.println(value); // 45
		
		
		double d = Double.parseDouble("45.6");
		System.out.println(d);// 45.6
	}
}
