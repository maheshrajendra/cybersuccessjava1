
public class SimpleProgram {

	static int min(int a, int b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
		// return (a<b)?a:b;
	}

	static String concat(String str1, String str2) {
		return str1 + " " + str2;
	}

	public static void main(String[] args) {
		System.out.println(min(10, 20)); // 10

		int result = min(65, 45);
		System.out.println(result);

		String output = concat("Hello", "World");
		System.out.println(output);
	}

}
