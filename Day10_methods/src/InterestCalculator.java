
public class InterestCalculator {
	static double findSimpleInterest(double p, int t, double r) {
		return (p * t * r) / 100;
	}

	public static void main(String[] args) {
		double interest = findSimpleInterest(100000.50, 1, 5.5);
		System.out.println("Your annual interest is "+ interest);
	}
}
