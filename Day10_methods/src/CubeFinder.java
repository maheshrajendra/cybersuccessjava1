
public class CubeFinder {
	static int cube(int n) {
		return n * n * n;
	}

	public static void main(String[] args) {
		int res = cube(5);
		System.out.println(res);
		System.out.println(res);
		System.out.println(res); // 125

		System.out.println(cube(35)); // 42875

		System.out.println(cube(4)); // 64
	}
}
