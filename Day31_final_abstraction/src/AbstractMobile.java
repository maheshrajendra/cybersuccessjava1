
public abstract class AbstractMobile {
	void switchOn() {
		System.out.println("Switched on the mobile phone");
	}
	
	void switchOff() {
		System.out.println("Switched off the mobile phone");
	}
	
	abstract void torchOn();

}
