
public class Narrowing {
	public static void main(String[] args) {
		long l = 135;
		System.out.println(l); // 135

		byte b = (byte)l;
		System.out.println(b); // -121
	}
}
