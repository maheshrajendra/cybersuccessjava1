package com.mr.zooexample;

public class Zoo {
	Animal openGate(String animalName) {
		if (animalName.equalsIgnoreCase("dog")) {
			Dog d = new Dog();
			d.color = "White";
			d.type = "Domestic";
			return d;
		} else if (animalName.equalsIgnoreCase("cat")) {
			Cat c = new Cat();
			c.color = "Black";
			c.type = "Wild";
			c.hasClaws = true;
			return c;
		} else {
			return null;
		}
	}

	// you are main method
	public static void main(String[] args) {
		Zoo z = new Zoo();
		Animal a = z.openGate("Dog"); // Animal a = null;
		if (a != null) {
			System.out.println(a.color);
			System.out.println(a.type);
			a.eat();
			if (a instanceof Dog) {
				Dog d = (Dog) a;
				d.bark();
			} else if (a instanceof Cat) {
				Cat c = (Cat) a;
				System.out.println(c.hasClaws);
				c.meow();
			}
		}
		System.out.println("Completed program");
	}
}
