package com.mr.nullpointerexception;

public class Alexa {

	static void greet(String name) {
		if (name != null) {
			System.out.println("Hello " + name.toUpperCase());
		} else {
			System.out.println("Hello");
		}
	}

	public static void main(String[] args) {
		Alexa.greet(null);
	}
}
