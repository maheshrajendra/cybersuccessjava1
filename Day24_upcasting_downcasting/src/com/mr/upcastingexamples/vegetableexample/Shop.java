package com.mr.upcastingexamples.vegetableexample;

class Vegetable {

}

class Tomato extends Vegetable {

}

class Potato extends Vegetable {

}

class Carrot extends Vegetable {

}

public class Shop {
	Vegetable sell(int opt) {
		if (opt == 1) {
			System.out.println("Returning tomato object");
			return new Tomato();
		} else if (opt == 2) {
			System.out.println("Returning carrot object");
			return new Carrot();
		} else {
			System.out.println("Returning potato object");
			return new Potato();
		}
	}

	public static void main(String[] args) {
		Shop s = new Shop();
		Vegetable v = s.sell(1); // Vegetable v = new Potato();
		System.out.println(v);
	}
}
