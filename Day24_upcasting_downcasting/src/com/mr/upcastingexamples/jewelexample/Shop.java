package com.mr.upcastingexamples.jewelexample;

public class Shop {
	Jewel sell(String name) {
		if (name.equals("ring")) {
			return new Ring();
		} else if (name.equals("bracelet")) {
			return new Bracelet();
		} else if (name.equals("chain")) {
			return new Chain();
		} else {
			return null;
		}
	}

	public static void main(String[] args) {
		Shop s = new Shop();
		Jewel j = s.sell("ring"); // Jewel j = new Ring();
		System.out.println(j);
	}
}

class Jewel {

}

class Ring extends Jewel {

}

class Bracelet extends Jewel {

}

class Chain extends Jewel {

}
