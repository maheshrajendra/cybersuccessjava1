package org.rj.zooapp;

public class TestAnimal {

	public static void main(String[] args) {
		Dog d1 = new Dog();
		d1.bark();

		Dog d2 = new Dog();
		d2.eat();
		System.out.println(d2.color);// null
		d2.color = "Black";
		System.out.println(d2.color); // Black

		System.out.println(d1.color); // null
	}
}
