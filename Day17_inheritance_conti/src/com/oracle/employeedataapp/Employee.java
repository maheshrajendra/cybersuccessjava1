package com.oracle.employeedataapp;

public class Employee {
	int age;
	double salary;
	char gender;
	String ename;
	long phoneNo;

	public Employee(String ename, int age, long phoneNo, char gender, double salary) {
		this.ename = ename;
		this.age = age;
		this.phoneNo = phoneNo;
		this.gender = gender;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [ename=" + ename + ", phoneNo=" + phoneNo + "]";
	}

	public static void main(String[] args) {
		Employee e = new Employee("Alpha", 23, 876255262627L, 'M', 568768.0);
		Employee e2 = new Employee("Beta", 33, 2487364862386L, 'F', 58686867.0);
		System.out.println("\n" + e); //
		System.out.println("\n" + e2); //
	}

}
