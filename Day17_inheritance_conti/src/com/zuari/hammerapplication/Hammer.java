package com.zuari.hammerapplication;

public class Hammer {
	String size;
	double weight;
	String materialType;

	void hammering() {
		System.out.println("Hammering...");
	}

	// ALT + Shift + S + S -> to override toString()
	@Override
	public String toString() {
		return "Hammer [size=" + size + ", weight=" + weight + ", materialType=" + materialType + "]";
	}

	public static void main(String[] args) {
		Hammer h = new Hammer();
		System.out.println("Before changing the values");
		System.out.println(h);
		h.size = "Large";
		h.materialType = "Iron";
		h.weight = 2.5;
		System.out.println("\n\n\n\nAfter changing the values");
		System.out.println(h);
	}

	
}
