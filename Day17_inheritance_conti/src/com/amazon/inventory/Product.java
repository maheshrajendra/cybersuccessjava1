package com.amazon.inventory;

public class Product {
	String productName;
	long productId;
	double price;
	boolean isAvailable;
	String brand;

	// ALT + SHIFT + S + S -> Override toString();
	@Override
	public String toString() {
		return "Product [brand=" + brand + "]";
	}

	public static void main(String[] args) {
		Product p1 = new Product();
		p1.brand = "LG";

		Product p2 = new Product();
		p2.brand = "Apple";

		Product p3 = new Product();
		p3.brand = "PineApple";

		Product p4 = new Product();
		p4.brand = "Samsung";

		System.out.println(p1); // Product [brand=LG] 
		System.out.println(p2); // Product [brand=Apple]
		System.out.println(p3); // Product [brand=PineApple]
		System.out.println(p4); // Product [brand=Samsung]
	}

}
