package com.amazon.inventory;

public class Customer {
	String name;
	long phoneNumber;

	// 2 param custom constructor
	Customer(String name, long phoneNumber) {
		this.name = name;
		this.phoneNumber = phoneNumber;
	}
	
	// override toString() -> ALt + SHIFT + S + S
	@Override
	public String toString() {
		return "Customer [phoneNumber=" + phoneNumber + ", name=" + name + "]";
	}
	
	public static void main(String[] args) {
		Customer c = new Customer("Alpha", 9865432345L);
		System.out.println(c); // instead of address it will print details of the customer
								//Customer [phoneNumber=9865432345, name=Alpha]
	}
}
