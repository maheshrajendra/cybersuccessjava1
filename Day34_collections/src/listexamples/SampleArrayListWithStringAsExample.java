package listexamples;

import java.util.ArrayList;

public class SampleArrayListWithStringAsExample {
	public static void main(String[] args) {
		ArrayList<String> colorList = new ArrayList<>();
		colorList.add("Red");
		colorList.add("Blue");
		colorList.add("Black");
		colorList.add("White");
		colorList.add("Yellow");
		System.out.println(colorList);

		for (String s : colorList) {
			System.out.println(s);
		}
	}
}
