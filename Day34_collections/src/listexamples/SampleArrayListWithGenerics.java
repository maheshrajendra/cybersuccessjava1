package listexamples;

import java.util.ArrayList;

public class SampleArrayListWithGenerics {
	public static void main(String[] args) {
		// <> it is called generic, it is used to make a collection homogenous type
		ArrayList<Integer> marks = new ArrayList<>();
		marks.add(45);
		marks.add(56);
		marks.add(100);
		marks.add(87);
		marks.add(34);
		marks.add(null);
		System.out.println(marks.size()); // 6
		System.out.println(marks.get(4)); // 34
		
		ArrayList<Integer> marksBackup = new ArrayList<>();
		marksBackup.addAll(marks);
		System.out.println(marksBackup); // [45,56,100,87,34, null]
	}
}
