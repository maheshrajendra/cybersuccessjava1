package listexamples;

import java.util.ArrayList;

public class SampleArrayListExample {
	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		al.add(45);
		al.add(4.5);
		al.add("Hello");
		al.add(true);
		al.add('H');
		System.out.println(al.size()); // 5
		System.out.println(al); // [45, 4.5, Hello, true, H]
		al.remove("Hello");
		System.out.println(al); // [45, 4.5, true, H]
		System.out.println(al.get(3)); // H
		al.set(0, 55);
		System.out.println(al); // [55, 4.5, true, H]
		System.out.println(al.isEmpty()); // false
		System.out.println(al.contains('H')); // true
		al.clear();
		System.out.println(al); // []
		System.out.println(al.size()); // 0
		System.out.println(al.isEmpty()); // true
	}
}
