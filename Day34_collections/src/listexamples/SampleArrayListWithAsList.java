package listexamples;

import java.util.ArrayList;
import java.util.Arrays;
public class SampleArrayListWithAsList {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5));
		System.out.println(list.size()); // 5
		System.out.println(list); // [1, 2, 3, 4, 5]
	}
}
