package listexamples;

import java.util.ArrayList;
import java.util.Arrays;

public class Student {
	int id;
	String name;
	int age;
	long phoneNo;

	// constructor - ALT + SHIFT + S + O
	public Student(int id, String name, int age, long phoneNo) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.phoneNo = phoneNo;
	}

	// override the toString method - ALT + SHIFT + S +S
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", phoneNo=" + phoneNo + "]\n";
	}

	public static void main(String[] args) {
		Student s1 = new Student(1, "Alpha", 20, 94323424234L);
		Student s2 = new Student(2, "Beta", 22, 2354876324863L);
		Student s3 = new Student(3, "Charlie", 21, 23423423423L);
		Student s4 = new Student(4, "Delta", 18, 453534534545L);
		Student s5 = new Student(5, "Mark", 25, 43534543535L);
		Student s6 = new Student(6, "Tyler", 24, 987294379274L);

		// ArrayList<Student> list = new ArrayList<>(Arrays.asList(s1,s2,s3,s4,s5,s6));
		ArrayList<Student> list = new ArrayList<>();
		list.add(s1);
		list.add(s2);
		list.add(s3);
		list.add(s4);
		list.add(s5);
		list.add(s6);

		System.out.println(list);

		System.out.println(list.get(4)); // Student [id=5, name=Mark, age=25, phoneNo=43534543535]
	
		
		// displaying the details of the student one by one using for each loop
		for(Student s  :   list)
		{
			System.out.println(s);
		}
		
	}

}
